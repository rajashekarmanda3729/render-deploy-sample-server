const express = require('express')
const app = express()
const dotenv = require('dotenv')
dotenv.config()
const PORT = process.env.PORT || 5000
const connectdb = require('./config/mongodb')
const colors = require('colors')

// connectdb()

app.use(express.json())
app.use(express.urlencoded({ extended: false }))

app.use('/api/users', require('./api/users'))


app.use((err, req, res, next) => {
    res.json({ response: err })
})

app.listen(PORT, () => console.log(`Server running on PORT: ${PORT}`))




