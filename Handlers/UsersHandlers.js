const axios = require('axios')
const UsersSchema = require('../Model/UsersModel')
const bcrypt = require('bcryptjs')
const jwtToken = require('jsonwebtoken')

// GET - api/users - LogIn User
const getUsers = async (req, res) => {
    const { email, password } = req.body
    // checking username/password
    if (!email || !password) {
        res.json({ response: 'Please provide email/password to login' })
    } else {

        const findUser = await UsersSchema.findOne({ email })
        if (findUser) {
            // validating password 
            const validate = await bcrypt.compare(password, existPassword)
            validate ? res.json({ token: findUser.token }) : res.json({ response: 'Wrong password entered' })
        } else {
            res.json({ response: 'User dosen\'t exist in db please register user' })
        }
    }
}

// POST - api/users - register user (creating new user)
const postUsers = async (req, res) => {

    const { name, email, password } = req.body

    // checking user details valid or not
    if (!name || !email || !password) {
        res.json({ response: 'User credentilas needed to create new user' })
    } else {

        // creating hassedpassword to user
        const salt = await bcrypt.genSalt(10)
        const hassedPassword = await bcrypt.hash(password, salt)

        // checking user-exist or not
        const findUser = await UsersSchema.findOne({ email })
        const token = jwtToken.sign({ email }, process.env.SECRET_KEY, { expiresIn: '90d' })
        if (!findUser) {
            const mongoResponse = await UsersSchema.create({
                token,
                name,
                email,
                password: hassedPassword,
            })
            res.json(mongoResponse.token)
        } else {
            res.json({ response: 'User already exist in db with same credentials' })
        }
    }
}

const putUsers = async (req, res) => {
    const reqToken = req.headers['authorization'].split(' ')[1]
    if (reqToken) {
        jwtToken.verify(reqToken, process.env.SECRET_KEY, (err, user) => {
            if (err) {
                res.sendStatuts(403)
            } else {
                res.json(user)
            }
        })
    } else {
        res.json({response:'Not authenticated user to '})
    }
}

const deleteUsers = async (req, res) => {
    res.json({ response: 'response delete' })
}

module.exports = {
    getUsers,
    postUsers,
    putUsers,
    deleteUsers
}

