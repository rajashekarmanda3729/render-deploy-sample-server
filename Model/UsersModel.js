const mongoose = require('mongoose')

const UsersSchema = mongoose.Schema({
    name: {
        type: String,
        required: [true, 'Please fill name field']
    },
    email: {
        type: String,
        required: [true, 'Please fill email field']
    },
    password: {
        type: String,
        required: [true, 'Please fill password field']
    },
    token: {
        type: String,
    },
}, {
    timestamps: false
})

module.exports = mongoose.model('UsersData', UsersSchema)


