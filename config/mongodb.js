const mongoose = require('mongoose')

const connectdb = async () => {
    try {
        const conn = await mongoose.connect(process.env.MONGO_URI)
        console.log(`Mongoose connected on ${conn.connection.host}`.cyan.underline)
    } catch (error) {
        console.log('Error on connecting mongoosedb')
        process.exit(1)
    }
}

module.exports = connectdb